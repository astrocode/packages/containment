# Containment

Helpers for `UIViewController` containment.

Use these to make views composable.

Based on [code by Dave DeLong](https://github.com/davedelong/MVCTodo).
