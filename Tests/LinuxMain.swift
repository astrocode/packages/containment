import XCTest

import ContainmentTests

var tests = [XCTestCaseEntry]()
tests += ContainmentTests.allTests()
XCTMain(tests)
