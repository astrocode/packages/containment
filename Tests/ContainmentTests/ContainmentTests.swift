import XCTest
@testable import Containment

final class ContainmentTests: XCTestCase {
    func testExample() {

    }

    static var allTests = [
        ("testExample", testExample),
    ]
}

// Really just making sure this compiles...
class MyContentViewController: ContainerViewController {
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}
