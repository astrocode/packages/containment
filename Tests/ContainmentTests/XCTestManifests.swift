import XCTest

#if !canImport(ObjectiveC)
open func allTests() -> [XCTestCaseEntry] {
    return [
        testCase(ContainmentTests.allTests),
    ]
}
#endif
