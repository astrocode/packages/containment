//
//  ContainerViewController.swift
//  MVCTodo
//
//  Created by Dave DeLong on 10/18/18.
//  Copyright © 2018 Syzygy. All rights reserved.
//

#if canImport(UIKit)

import UIKit

private class BlankViewController: UIViewController {
    init() {
        super.init(nibName: nil, bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        view.alpha = 0
    }
}

open class ContainerViewController: UIViewController {

    // flag to set the initial content only the first time the view appears.
    private var hasSetInitialContent = false

    private var deferredTransitionHandler: (() -> Void)?

    open var content: UIViewController {
        didSet {
            replaceChild(oldValue, with: content)

            setNeedsStatusBarAppearanceUpdate()
            setNeedsUpdateOfHomeIndicatorAutoHidden()
            setNeedsUpdateOfScreenEdgesDeferringSystemGestures()
            if #available(iOS 14.0, *) {
                setNeedsUpdateOfPrefersPointerLocked()
            }
        }
    }

    public init(content: UIViewController?) {
        self.content = content ?? BlankViewController()
        super.init(nibName: nil, bundle: nil)
    }

    public convenience init() {
        self.init(content: nil)
    }

    public required init?(coder: NSCoder) {
        self.content = BlankViewController()
        super.init(coder: coder)
    }

    open override func loadView() {
        view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = true
        view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        if #available(iOS 13.0, *) {
            view.backgroundColor = .systemBackground
        } else {
            view.backgroundColor = .white
        }
    }

    open override var childForStatusBarStyle: UIViewController? { content }
    open override var childForStatusBarHidden: UIViewController? { content }
    open override var childForHomeIndicatorAutoHidden: UIViewController? { content }
    open override var childForScreenEdgesDeferringSystemGestures: UIViewController? { content }
    open override var childViewControllerForPointerLock: UIViewController? { content }

    open override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        // special case for setting the initial content. works around some "unbalanced calls" to appear/disappear methods in the child.
        if !hasSetInitialContent {
            addChild(content)
            content.didMove(toParent: self)
            view.addSubview(content.view)
            content.view.frame = view.bounds
            content.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]
            hasSetInitialContent = true
        }
    }

    private func replaceChild(_ oldChild: UIViewController, with newChild: UIViewController) {
        guard hasSetInitialContent else { return }

        if oldChild.isBeingPresented || oldChild.isBeingDismissed {
            // prevent VC presentation errors when the content VC is changed mid-transition
            deferredTransitionHandler = { [weak self] in
                self?.replaceChild(oldChild, with: newChild)
            }
            return
        }

        let duration: TimeInterval
        let options: UIView.AnimationOptions

        if view?.window == nil {
            duration = 0
            options = []
        } else {
            duration = 0.35
            options = [.transitionCrossDissolve, .beginFromCurrentState]
        }

        transition(from: oldChild, to: newChild, in: view, duration: duration, options: options)
    }

    private func transition(from: UIViewController, to: UIViewController, in container: UIView, duration: TimeInterval, options: UIView.AnimationOptions) {
        guard from != to else { return }

        // animate from "from" view to "to" view

        from.beginAppearanceTransition(false, animated: true)
        to.beginAppearanceTransition(true, animated: true)

        from.willMove(toParent: nil)
        addChild(to)

        to.view.alpha = 0
        from.view.alpha = 1

        container.addSubview(to.view)
        to.view.frame = container.bounds
        to.view.autoresizingMask = [.flexibleWidth, .flexibleHeight]

        container.bringSubviewToFront(from.view)

        let runNextTransition = { [weak self] in
            self?.deferredTransitionHandler?()
            self?.deferredTransitionHandler = nil
        }

        UIView.animate(withDuration: duration, delay: 0, options: options, animations: {
            to.view.alpha = 1
            from.view.alpha = 0
        }, completion: { finished in
            from.view.removeFromSuperview()

            from.endAppearanceTransition()
            to.endAppearanceTransition()

            from.removeFromParent()
            to.didMove(toParent: self)

            if finished {
                runNextTransition()
            }
        })
    }
}

#endif
